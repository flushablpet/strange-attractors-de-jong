use itertools::Itertools;
use nannou::image::DynamicImage;
use nannou_egui::{self, Egui};
use super::attractor::{Attractor, coefficients::Coefficients};
use nannou::prelude::*;

pub struct SceneColor {
    pub background: Rgb,
    pub foreground: Rgb
}

pub struct Model {
    pub attractor: Attractor,
    pub gui: Egui,
    pub image: DynamicImage,
    pub scene: SceneColor,
    pub dimension: usize
}

impl Model {
    pub fn new(scene: SceneColor, dimensions: usize, gui: Egui) -> Self {

      Self {
        attractor: Attractor::new(
            Coefficients::new(-1.10, 1.695, 2.296, -1.1),
            3.0,
            dimensions
        ),
        scene,
        gui,
        image: DynamicImage::new_rgba8(dimensions as u32, dimensions as u32),
        dimension: dimensions
      }
    }

    pub fn draw(&mut self) {
        self.attractor.set_gamma();
        self.draw_pixel();
    }

    fn draw_pixel(&mut self) {
        for (x, y) in (0..self.dimension)
            .cartesian_product(0..self.dimension)
        {
            let (r, g, b) = self.attractor.histo_buckets[x][y].color;
            let freq = self.attractor.histo_buckets[x][y].frequency;
            if freq == 0 {
                continue;
            }

            let alpha_log = map_range(self.attractor.histo_buckets[x][y].normal_pixel, 0., 4.5, 0., 0.5);
            if let Some(buffer) = self.image.as_mut_rgba8() {
                let hsl = nannou::color::hsla(r, g, b, alpha_log);
                //let rgb: Srgba<u8> = nannou::color::rgba(r, g, b, alpha_log).into_format();
                let r : Srgba = hsl.into();
                let rgb: Srgba<u8> = r.into_format();
                buffer.put_pixel(
                    x as u32,
                    y as u32,
                    nannou::image::Rgba::from([rgb.red, rgb.green, rgb.blue, rgb.alpha]));
            }
        }
    }
}
