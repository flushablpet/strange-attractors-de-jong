mod attractor;
mod model;

use chrono::{DateTime, Local};
use model::{Model, SceneColor};
use nannou::{prelude::*, wgpu::Texture};
use nannou_egui::{self, egui, Egui};

// BLEND_NORMAL, BLEND_ADD, BLEND_SUBTRACT, BLEND_REVERSE_SUBTRACT, BLEND_DARKEST, BLEND_LIGHTEST
const BLEND_MODE: wgpu::BlendComponent = BLEND_NORMAL;
const SCREEN_DIM: u32 = 2048;

fn main() {
    nannou::app(model)
    .update(update)
    .run();
}

fn model(app: &App) -> Model {
    let _window_image = app
        .new_window()
        .title("DeJong Strange Attractor")
        .size(SCREEN_DIM as u32, SCREEN_DIM as u32)
        .view(view)
        .key_pressed(key_pressed)
        .build()
        .unwrap();

    let window_controls = app
        .new_window()
        .title("Controls")
        .size(200, 400)
        .view(ui_view)
        .raw_event(raw_window_event)
        .build().unwrap();

    let window = app.window(window_controls).unwrap();
    let gui = Egui::from_window(&window);
    let scene = SceneColor {
        background: Rgb::new(0.9, 0.89, 0.97),
        foreground: Rgb::new(0.05, 0.05, 0.15)
    };

    Model::new(scene, SCREEN_DIM as usize, gui)
}

fn update(_app: &App, model: &mut Model, update: Update) {
    for _ in 0..10_000_000 {
        model.attractor.update();
    }
    model.draw();

    let gui = &mut model.gui;
    gui.set_elapsed_time(update.since_start);
    let ctx = gui.begin_frame();

    egui::Window::new("Settings").show(&ctx, |ui| {
        ui.label("Coefficients a, b, c, d");
        ui.add(egui::Slider::new(&mut model.attractor.coefficients.a, -2.5..=2.5));
        ui.add(egui::Slider::new(&mut model.attractor.coefficients.b, -2.5..=2.5));
        ui.add(egui::Slider::new(&mut model.attractor.coefficients.c, -2.5..=2.5));
        ui.add(egui::Slider::new(&mut model.attractor.coefficients.d, -2.5..=2.5));
        ui.add(egui::Separator::default());
        ui.label("Iterations");
        ui.add(egui::Label::new(format!("{:?}", model.attractor.iterations)));
    });
}

// Image view
fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw().color_blend(BLEND_MODE);

    if app.elapsed_frames() == 0 {
        draw.background().color(model.scene.background);

        let win_rect = app.window_rect();
        let padding = win_rect.pad(10.0);
        draw.text(&format!("{:?}; Iterations: {:?}", model.attractor.coefficients, model.attractor.iterations))
            .font_size(12)
            .wh(padding.wh())
            .left_justify()
            .align_text_top()
            .color(model.scene.foreground);
    }

    let texture = Texture::from_image(app, &model.image);
    draw.texture(&texture);
    draw.to_frame(app, &frame).unwrap();
}

// UI controls view
fn ui_view(_app: &App, model: &Model, frame: Frame) {
    model.gui.draw_to_frame(&frame).unwrap();
}

fn raw_window_event(_app: &App, model: &mut Model, event: &nannou::winit::event::WindowEvent) {
    model.gui.handle_raw_event(event);
}

fn key_pressed(app: &App, _model: &mut Model, key: Key) {
    let now = chrono::offset::Local::now();

    if key == Key::Space {
        let file_path = create_frame_path(app, now);
        app
        .main_window()
        .capture_frame(file_path);
    }
}

fn create_frame_path(app: &App, time: DateTime<Local>) -> std::path::PathBuf {
  app.project_path()
    .expect("Expected project path")
    .join("output")
    .join(format!("{}-{}", time.format("%Y-%m-%d %H-%M-%S"), SCREEN_DIM))
    .with_extension(".png")
}