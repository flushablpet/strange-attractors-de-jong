#[derive(Debug, Clone)]
pub struct Histogram {
    pub frequency: usize,
    pub color: (f32, f32, f32),
    pub alpha: f32,
    pub normal_pixel: f32
}

impl Histogram {
    pub const COLOR_R: f32 = 0.75;
    pub const COLOR_G: f32 = 0.8;
    pub const COLOR_B: f32 = 0.2;//0.8;

    pub fn default() -> Self {
        Self {
            frequency: 0,
            color: (Histogram::COLOR_R, Histogram::COLOR_G, Histogram::COLOR_B),
            alpha: 0.0,
            normal_pixel: 0.0
        }
    }
}