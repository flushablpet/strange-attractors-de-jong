#[derive(Debug)]
pub struct Coefficients {
    pub a: f32,
    pub b: f32,
    pub c: f32,
    pub d: f32,
}

impl Coefficients {
    pub fn new(a: f32, b: f32, c: f32, d: f32) -> Self {
        Self { a, b, c, d}
    }
}
