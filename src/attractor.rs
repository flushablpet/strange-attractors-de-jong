pub mod coefficients;
pub mod histogram;

use histogram::Histogram;
use coefficients::Coefficients;
use nannou::prelude::{Vec2, map_range, pt2};
use itertools::Itertools;

pub struct Attractor {
    origin: Vec2,
    distance: f32,
    gamma: f32,
    dimension: usize,
    pub coefficients: Coefficients,
    pub histo_buckets: Vec<Vec<Histogram>>,
    pub iterations: u64,
}

impl Attractor {

    /// Create a new attractor instance
    pub fn new(
        coefficients: Coefficients,
        gamma: f32,
        dimension: usize) -> Self {

        Self {
            origin: pt2(1.0, -1.0),
            distance: 0.0,
            gamma,
            dimension,
            coefficients,
            histo_buckets: vec![vec![Histogram::default(); dimension]; dimension],
            iterations: 0
        }
    }

    // Maps the vec to a pixel in the screen space.
    pub fn map_to_display_dims(&self, pt: Vec2, dimension: usize) -> (usize, usize) {
      (map_range(pt.x, -2.5, 2.5, 0, dimension - 1),
       map_range(pt.y, -2.5, 2.5, 0, dimension - 1))
    }

    // Increments the formula.
    fn step_x_y(&self) -> Vec2 {
        pt2(
            (self.coefficients.a * self.origin.y).sin() - (self.coefficients.b * self.origin.x).cos(),
            (self.coefficients.c * self.origin.x).sin() - (self.coefficients.d * self.origin.y).cos()
        )
    }

    fn update_pixel(&mut self, x: usize, y: usize) {
        let mut pixel = &mut self.histo_buckets[x][y];
        pixel.color = (
            (pixel.color.0 + Histogram::COLOR_R) / 2.0,
            (pixel.color.1 + Histogram::COLOR_G) / 2.0,
            (pixel.color.2 + Histogram::COLOR_B) / 2.0
        );
        pixel.frequency += 1;
    }

    fn calc_distance(&self, origin_prime: &Vec2) -> f32 {
        let (dx, dy) = (origin_prime.x - self.origin.x, origin_prime.y - self.origin.y);
        (dx * dx + dy * dy).sqrt()
    }

    // Updates the state of the attractor
    pub fn update(&mut self) {
        let origin_prime = self.step_x_y();
        self.distance = self.calc_distance(&origin_prime);
        self.origin = origin_prime;

        let (x, y) = self.map_to_display_dims(self.origin, self.dimension);
        self.update_pixel(x, y);
        self.iterations += 1;
    }

    // Sets the gamma for the pixel rgb values
    pub fn set_gamma(&mut self) {
        // Get the logarithmic maximum freq from all pixels
        let mut max = 0.;
        for (x, y) in (0..self.dimension)
        .cartesian_product(0..self.dimension) {
            if self.histo_buckets[x][y].frequency == 0 {
                continue;
            }

            let mut pixel = &mut self.histo_buckets[x][y];
            pixel.normal_pixel = ((pixel.frequency + 1) as f32).log10();
            max = pixel.normal_pixel.max(max);
        }

        // Adjust the gamma for each pixel's rgb components
        for (x, y) in (0..self.dimension)
        .cartesian_product(0..self.dimension) {
            let mut pixel = &mut self.histo_buckets[x][y];
            pixel.normal_pixel /= max;

            pixel.color.0 *= pixel.normal_pixel.powf(1.0 / self.gamma);
            pixel.color.1 *= pixel.normal_pixel.powf(1.0 / self.gamma);
            pixel.color.2 *= pixel.normal_pixel.powf(1.0 / self.gamma);
        }
    }
}